import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

public class Wxhw
{
	public String getWx(String stationId)
	{
		JsonElement jse = null;
      String wxReport = "";

		try
		{
			// Construct WxStation API URL
			URL wxURL = new URL("http://api.openweathermap.org/data/2.5/weather?zip="
					+ stationId
					+ "&units=imperial&appid=8ce2b173377f00c76ee0a43cccf99aeb");

			// Open the URL
			InputStream is = wxURL.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);
      
			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.FileNotFoundException fne)
		{
			//fne.printStackTrace();
         System.out.println("Error: no cities match your search query");
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
         try 
         {
            // Build a weather report
            String name = jse.getAsJsonObject().get("name").getAsString();
            wxReport = wxReport + "Location:       " + name + "\n";
      
            String time = jse.getAsJsonObject().get("dt").getAsString();
            java.util.Date tt = new java.util.Date(Long.parseLong(time)*1000);
            wxReport = wxReport +  "Time:           Last Updated on " + tt + "\n";
            
                  
            JsonArray weather = jse.getAsJsonObject().get("weather").getAsJsonArray();
            String conditions = weather.get(0).getAsJsonObject().get("description").getAsString();
            wxReport = wxReport + "Weather:        " + conditions + "\n";

            JsonElement t = jse.getAsJsonObject().get("main").getAsJsonObject();
            String temp = t.getAsJsonObject().get("temp").getAsString(); 
            wxReport = wxReport + "Temperature F:  " + temp + "\n";

            JsonElement w = jse.getAsJsonObject().get("wind").getAsJsonObject();
            JsonElement d = jse.getAsJsonObject().get("wind").getAsJsonObject();
            String wind = w.getAsJsonObject().get("speed").getAsString();
            String deg = d.getAsJsonObject().get("deg").getAsString();
            //int degree = Integer.parseInt(deg);
            wxReport = wxReport + "Wind:           From  " + deg + " degrees at " + wind + " MPH" + "\n";
                                          
            JsonElement obs = jse.getAsJsonObject().get("main").getAsJsonObject();
            String pressure = obs.getAsJsonObject().get("pressure").getAsString();
            wxReport = wxReport + "Pressure in HG: " + pressure + "\n";
        }
        catch (java.lang.IllegalStateException ise)
   	  {
   			ise.printStackTrace();
   	  }    
		}
    return wxReport;
	}
	public static void main(String[] args) 
	{
      if(args.length == 0)
      {
         System.out.println("Enter a location: ");
         System.exit(1);
      }
      String location = args[0];
		String b = new Wxhw().getWx(location);
      if ( b != null )
		  System.out.println(b);
	}
}
